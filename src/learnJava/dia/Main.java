
package learnJava.dia;

import java.util.Scanner;


public class Main{

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String word;

        do {
            System.out.print("Enter word or 'exit' for quit from program : ");
            word = scan.nextLine();

            if (isPalindrome(word)) {
                System.out.println(word + " - Is Palindrome!");
            }

        }while (!word.equalsIgnoreCase("exit"));

    }

    private static boolean isPalindrome(String word){
        word = word.replaceAll("[^a-zA-Z0-9]", "");
        char[] chars = word.toCharArray();

        for(int b = 0, e = chars.length - 1; b < e; ++b, --e){
            if(chars[b] != chars[e]){
                return false;
            }
        }

        return (true && chars.length > 0);
    }

}
