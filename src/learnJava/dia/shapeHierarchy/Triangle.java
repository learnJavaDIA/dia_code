
package learnJava.dia.shapeHierarchy;

import learnJava.dia.Point2D;


class Triangle extends Shape{

    private Point2D p[];

    public Triangle(Point2D a, Point2D b, Point2D c, Color color){
        super(color);
        p = new Point2D[3];
        p[0] = a;
        p[1] = b;
        p[2] = c;
    }
    public Triangle(){
        this(new Point2D(-1, 0), new Point2D(0, 1), new Point2D(1, 0), Color.BLACK);
    }

    public String toString(){
        return "A : " + p[0] + "\n" +
                "B : " + p[1] + "\n" +
                "C : " + p[2] + "\n" + super.toString();
    }


    public double area() {
        return Math.abs(0.5 * ((p[0].getX() - p[2].getX()) * (p[1].getY() - p[2].getY()) - (p[1].getX() - p[2].getX()) * (p[0].getY() - p[2].getY())));
    }

}