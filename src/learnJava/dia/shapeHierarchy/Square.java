
package learnJava.dia.shapeHierarchy;

import learnJava.dia.Point2D;

class Square extends Shape{
    private Point2D point;
    private double side;

    public Square(Point2D point, double side, Color color){
        super(color);
        this.point = point;
        this.side = side;
    }
    public Square(){
        this(new Point2D(0, 0), 1, Color.BLACK);
    }

    public String toString(){
        return "Point : " + point.toString() + "\n" +
                "Side : " + side + "\n" + super.toString();
    }


    public double area(){
        return (side * side);
    }
}