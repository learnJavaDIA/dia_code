
package learnJava.dia.shapeHierarchy;

abstract class Shape{
    private Color color;

    public Shape(Color color){
        this.color = color;
    }
    public Shape(){
        color = Color.BLACK;
    }

    public String toString(){
        return "Color : " + color.toString();
    }


    public abstract double area();

}