
package learnJava.dia.shapeHierarchy;

import learnJava.dia.Point2D;


class Circle extends Shape{

    private Point2D center;
    private double radius;

    public Circle(Point2D center, double radius, Color color){
        super(color);
        this.center = center;
        this.radius = radius;
    }
    public Circle(){
        this(new Point2D(0, 0), 1, Color.BLACK);
    }

    public String toString(){
        return "Center : " + center.toString() + "\n" +
               "Radius : " + radius + "\n" + super.toString();
    }


    public double area(){
        return (3.1415926535 * radius * radius);
    }

    public Point2D getCenter() {
        return center;
    }
    public double getRadius() {
        return radius;
    }

}