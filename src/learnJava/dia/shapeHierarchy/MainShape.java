
package learnJava.dia.shapeHierarchy;

import learnJava.dia.Point2D;


public class MainShape{
    public static void main(String[] args) {
        Circle circle = new Circle(new Point2D(), 5, Color.RED);
        Square square = new Square(new Point2D(), 10, Color.BLUE);
        Triangle triangle = new Triangle();

        System.out.println(circle);
        System.out.println(square);
        System.out.println(triangle);

        System.out.println("Circle area = " + circle.area());
        System.out.println("Square area = " + square.area());
        System.out.println("Triangle area = " + triangle.area());

    }
}