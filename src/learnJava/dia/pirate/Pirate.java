/**
 * Класс Pirate выполняет поиск всех возможных значений
 * при данных условиях в диапазоне (7; 10000).
 *
 * @author D.I.A.
 */

package learnJava.dia.pirate;


public class Pirate{

    public static void main(String[] args) {

        int i = 7;

        while (i <= 10000){

            if(isRight(i)){

                System.out.println(i);

            }

            ++i;

        }
    }


    /**
     * Метод определяет, соответствует ли число таким требованиям:
     * делится на 2-6 с остатком 1 и делится на 7 без остатка.
     *
     * @param n - проверяемое число.
     * @return true - в случае если соответствует требованиям, иначе - false.
     */
    private static boolean isRight(int n){

        for(int i = 2 ; i < 7; ++i){

            if((n % i) != 1){

                return false;

            }

        }

        return ((n % 7) == 0);

    }

    /*
     * Метод определяет, соответствует ли число заданным условиям.
     *
     * @param i - проверяемой значение.
     * @return true - в случае если соответствует требованиям, иначе - false.

    private static boolean isRight(int i){

        int q = 2;
        for( ; q < 7; ++q){

            if((i - (i / q) * q) != 1){

                return false;

            }

        }

        return ((float)i % (float)q == 0);

    }*/

}