package learnJava.dia.solvesForRepeat.Task14;

public class Task_14 {

    public static void main(String[] args) {

        int matrix[][] = {
                {1, 2, 3, 8},
                {4, 5, 6, 9},
                {7, 8, 9, 10},
                {12, 34, 32, 1}
        };

        printMatrix(matrix);

        transpon(matrix);

        printMatrix(matrix);

    }

    private static  void printMatrix(int matrix[][]){
        for(int row = 0; row < matrix.length; ++row){
            for(int col = 0; col < matrix[0].length; ++col){
                System.out.print(matrix[row][col] + "|");
            }
            System.out.println();
        }
    }

    private static void transpon(int matrix[][]){
        int b = 0;

        if(matrix.length == matrix[0].length){
            for(int row = 0; row < matrix.length; ++row){
                for(int col = 1 + row; col < matrix[0].length; ++col){
                    b = matrix[col][row];
                    matrix[col][row] = matrix[row][col];
                    matrix[row][col] = b;
                }
            }
        }
    }

}