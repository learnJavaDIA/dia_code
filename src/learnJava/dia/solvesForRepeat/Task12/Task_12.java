package learnJava.dia.solvesForRepeat.Task12;

public class Task_12 {

    public static void main(String[] args) {

        int matrix[][] = {
                {123, 5, 211},
                {-12, 521, 4},
                {9, 232, 163}
        };

        int line[] = new int[matrix.length * matrix[0].length];


        for(int row = 0, i = 0; row < matrix.length; ++row){
            for(int col = 0; col < matrix[0].length; ++col, ++i){
                line[i] = matrix[row][col];
            }
        }


        for(int row = 0; row < matrix.length; ++row){
            for(int col = 0; col < matrix[0].length; ++col){
                System.out.print(matrix[row][col] + "|");
            }

            System.out.println();
        }


        for(int i = 0; i < line.length; ++i){
            System.out.print(line[i] + "|");
        }

    }
}