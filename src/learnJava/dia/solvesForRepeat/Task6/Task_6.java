package learnJava.dia.solvesForRepeat.Task6;

import java.util.Scanner;

public class Task_6 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        byte number = 0;

        do{

            System.out.println("inter number 1 - 10");
            number = scan.nextByte();

        }while(number < 1 && number > 10);

        for(int i = 1; i < 11; ++i){
            System.out.printf("%d * %d = %d\n", number, i, number * i);
        }


    }
}