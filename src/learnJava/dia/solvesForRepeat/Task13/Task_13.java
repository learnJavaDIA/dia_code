package learnJava.dia.solvesForRepeat.Task13;

import java.util.Scanner;

public class Task_13 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter symbol : ");

        String symbol = scan.next();

        symbol = symbol.replaceAll("[a-zA-Z]", " ");

        if(symbol.charAt(0) >= '0' && symbol.charAt(0) <= '9'){
            System.out.println("is digit");
        }else if(symbol.charAt(0) == ' '){
            System.out.println("is letter");
        }else {
            System.out.println("is punctuation sign");
        }

    }
}