package learnJava.dia.solvesForRepeat.Task7;

import java.util.Scanner;

public class Task_7 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        double number = 0;

        System.out.print("Enter number : ");
        number = scan.nextDouble();

        if(number % 1 == 0){
            System.out.println("Это целое число");
        }else {
            System.out.println("Это дробное число");
        }
    }
}