package learnJava.dia.solvesForRepeat.Task3;

public class Task_3{

    public static void main(String[] args) {

        double balance = 2134;
        double course = 56.23;

        System.out.println("balance " + balance + " rub");
        System.out.println("balance " + translateRubToEuro(balance, course) + " euro");
    }

    private static double translateRubToEuro(double rub, double course){
        if(course > 0){
            return rub / course;
        }else{
            System.out.println("invalid value");
            return 0;
        }
    }
}