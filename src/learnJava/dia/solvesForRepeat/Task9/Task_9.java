package learnJava.dia.solvesForRepeat.Task9;

public class Task_9 {

    public static void main(String[] args) {

        int number = 34443;

        if(isPolimdrome(number)){
            System.out.println("Polindrome");
        }else{
            System.out.println("Not Polindrome");
        }

    }

    private static boolean isPolimdrome(int number){
        char chars[] = Integer.toString(number).toCharArray();
        for(int b = 0, e = chars.length - 1; b < e; ++b, --e){
            if(chars[b] != chars[e]){
                return false;
            }
        }

        return true;
    }
}