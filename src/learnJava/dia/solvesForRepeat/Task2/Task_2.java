
package learnJava.dia.solvesForRepeat.Task2;


public class Task_2{

    public static void main(String[] args) {

        double array[] = {12, 22, 190, 100};

        System.out.println("before method : " + array[3]);

        toEnlargeOnTenProcent(array, 3);

        System.out.println("after method : " + array[3]);
    }

    /**
     * Метод выполняет увеличение указанного элемента массива arr на 10 процентов.
     * @param arr, массив элемент которого требуется увеличить.
     * @param index, индекс элемента который требуется увелисить.
     */
    private static void toEnlargeOnTenProcent(double arr[], int index){
        if(index > -1 && index < arr.length){
            arr[index] += (arr[index] * 0.1);
        }
    }
}