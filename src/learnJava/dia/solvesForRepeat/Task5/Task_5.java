package learnJava.dia.solvesForRepeat.Task5;

public class Task_5 {

    public static void main(String[] args) {

        boolean isSimpleNumber = false;

        for(double i = 2; i != 100;  i += 1){

            for(double q = i - 1; q > 1; q -= 1){
                isSimpleNumber = (i % q) != 0;
                if(!isSimpleNumber){
                    break;
                }
            }

            if(isSimpleNumber){
                System.out.println(i);
            }

        }

    }
}