package learnJava.dia.solvesForRepeat.Task11;

import java.util.Scanner;

public class Task_11 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int days = 0;

        System.out.print("Введите количество суток : ");
        days = scan.nextInt();

        if(days > -1){
            int hours = days * 24;
            int minute = hours * 60;
             System.out.printf("В %d сутках\n%d часов, %d минут, %d секунд", days, hours, minute, minute * 60);
        }else {
            System.out.println("Недопустимое значение!");
        }

    }
}