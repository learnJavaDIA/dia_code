package learnJava.dia.solvesForRepeat.Task10;

public class Task_10 {

    public static void main(String[] args) {

        String str = "helleh";

        if(isPolimdrome(str)){
            System.out.println("Polindrome");
        }else{
            System.out.println("Not Polindrome");
        }

    }

    private static boolean isPolimdrome(String str){
        char chars[] = str.toCharArray();
        for(int b = 0, e = chars.length - 1; b < e; ++b, --e){
            if(chars[b] != chars[e]){
                return false;
            }
        }

        return true;
    }
}