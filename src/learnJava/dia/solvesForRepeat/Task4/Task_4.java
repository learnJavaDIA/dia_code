package learnJava.dia.solvesForRepeat.Task4;

public class Task_4 {

    private static final double SOUND_SPEED = 1234.8;
    private static final double SEC_IN_HOUR = 3600;

    public static void main(String[] args) {

        double interval = 6.8;

        System.out.println("distance = " + (SOUND_SPEED * interval / SEC_IN_HOUR) + " km");

    }
}