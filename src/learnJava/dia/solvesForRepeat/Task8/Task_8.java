package learnJava.dia.solvesForRepeat.Task8;

import java.util.Scanner;

public class Task_8 {

    private static int matrix[][] = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
    };

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int number = 0;

        printMatrix();

        System.out.print("Enter number : ");
        number = scan.nextInt();

        if(number > -1 && number < matrix[0].length){
            zero(number);
            printMatrix();
        }else {
            System.out.println("Такого столбца не существует");
        }
    }

    private static void printMatrix(){
        for(int row = 0; row < matrix.length; ++row){
            for(int col = 0; col < matrix[row].length; ++col){
                System.out.print(matrix[row][col]);
                System.out.print("|");
            }
            System.out.println();
        }
    }

    private static void zero(int column){
        for(int row = 0; row < matrix.length; ++row){
            matrix[row][column] = 0;
        }
    }
}