
package learnJava.dia.solvesForRepeat.Task1;

import java.util.Scanner;

public class Task_1{

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        StringBuilder strBuilder = new StringBuilder();
        boolean isDot = false;

        do{

            String str = scan.nextLine();
            int len = str.length();

            for(int i = 0; i < len; ++i){
                isDot = str.charAt(i) == '.';
                if(isDot){
                    break;
                }else{
                    if(str.charAt(i) != ' ' || (i > 0 && str.charAt(i - 1) != ' ')){
                        strBuilder.append(str.charAt(i));
                    }
                }
            }

        }while (!isDot);

        System.out.println(strBuilder.toString());
    }
}