package dia.frequencySort;


import java.io.*;
import java.util.ArrayList;


public class Main {


    private static String IN_FILE_PATH  = "C:\\Users\\DatLik\\IdeaProjects\\DIA_Code\\src\\learnJava\\dia\\taskSort\\input.txt";
    private static String OUT_FILE_PATH = "C:\\Users\\DatLik\\IdeaProjects\\DIA_Code\\src\\learnJava\\dia\\taskSort\\output.txt";

    public static void main(String[] args) throws IOException {

        long startTime = System.currentTimeMillis();

        BufferedReader bufferedReader = new BufferedReader(new FileReader(IN_FILE_PATH));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(OUT_FILE_PATH));

        String symbols[] = bufferedReader.readLine().split(" ");
        int array[] = new int[symbols.length];

        for(int i = 0; i < symbols.length; ++i){

            array[i] = Integer.parseInt(symbols[i]);

        }

        frequencySort(array);
        //bubbleSort(array);

        printArray(array);

        for(int i = 0; i < array.length; ++i){

            bufferedWriter.write(Integer.toString(array[i]));
            bufferedWriter.write(" ");

        }

        bufferedReader.close();
        bufferedWriter.close();

        long runtime = System.currentTimeMillis() - startTime;
        System.out.printf("Runtime: %.2f sec\n", (runtime / 1000.0f), runtime);

    }

    private static void printArray(int[] array){

        for(int i = 0; i < array.length; ++i){

            System.out.printf("[%d]", array[i]);

        }

        System.out.println();

    }

    private static void frequencySort(int[] array){

        // Создание списка listArray и помещение в него всех элементов массива.
        ArrayList<Integer> listArray = new ArrayList<>();

        for(int i = 0; i < array.length; ++i){

            listArray.add(array[i]);

        }

        // Создание списка listValue для хранения данных типа Value
        ArrayList<Value> listValue = new ArrayList<>();

        // Формирование значиний типа Value из списка listArray
        //int q = 0;
        int curValue;
        int curAmount;

        while (!listArray.isEmpty()){

            curValue  = listArray.get(0);
            //System.out.printf("curValue = %d\n", curValue);
            curAmount = 0;

            for(int i = 0; i < listArray.size(); ){

                if(curValue == listArray.get(i)){

                    curAmount++;
                    listArray.remove(i);

                }else{

                    i++;

                }

            }

            listValue.add(new Value(curValue, curAmount));

        }

        Value valueArray[] = new Value[listValue.size()];
        listValue.toArray(valueArray);

        // bubble sort
        Value valueBuffer;
        for(int i = valueArray.length; i > 0; --i){

            for(int r = 1; r < i; ++r){

                valueBuffer = valueArray[r - 1];
                if(valueBuffer.amount < valueArray[r].amount){

                    valueArray[r - 1] = valueArray[r];
                    valueArray[r] = valueBuffer;

                }

            }

        }

        // writing in source array
        for(int i = 0, e = 0, t = 0; i < valueArray.length; ++i, t = 0){

            while (t < valueArray[i].amount){

                array[e] = valueArray[i].value;

                t++;
                e++;

            }

        }

    }

}

class Value{

    public int value;
    public int amount;


    public Value(int value, int amount){

        this.value  = value;
        this.amount = amount;

    }

    public Value(){

        value  = 0;
        amount = 0;

    }

}

