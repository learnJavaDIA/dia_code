
package learnJava.dia.calc;

/**
 * Класс тестирования упрощенного класса Math, Calc.
 *
 * @quthor D.I.Aleksandrovich.
 *
 */

import learnJava.dia.calc.Calc;
import java.util.Scanner;

public class CalcDemo{

    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args){
        double a, b; // Операнды вводимые пользователем.
        int operationCode; // Код выбранной пользователем операции.

        System.out.print("Введите число 'a' : ");
        a = scan.nextDouble();
        System.out.print("Введите число 'b' : ");
        b = scan.nextDouble();


        do{
            System.out.print("Выберите операцию:\n1 _ add\n2 _ sub\n3 _ multi\n4 _ divi\n5 _ divd\n" +
                    "6 _ remainder\n7 _ pow\n0 _ exit\n: ");

            operationCode = scan.nextInt();

            if(operationCode < 0 || operationCode > 7){
                System.out.println("Неправильно!");
            }

        }while (operationCode < 0 || operationCode > 7);

        switch (operationCode){
            case 1:
                System.out.println("a + b = " + Calc.add(a, b));
                break;
            case 2:
                System.out.println("a - b = " + Calc.sub(a, b));
                break;
            case 3:
                System.out.println("a * b = " + Calc.multi(a, b));
                break;
            case 4:
                System.out.println("a divi b = " + Calc.div((int)a, (int)b));
                break;
            case 5:
                System.out.println("a divd b = " + Calc.div(a, b));
                break;
            case 6:
                System.out.println("a % b = " + Calc.remainder(a, b));
                break;
            case 7:
                System.out.println("a ^ b = " + Calc.pow(a, (int)b));
                break;
            case 0:
                System.out.println("Выход!");
                break;
        }

    }
}