
package learnJava.dia.calc;

/**
 * <h1> Класс реализующий некоторый функционал </h1>
 * <h1> стандартного класса Math. </h1>
 *
 * <h3> Содержит следующие методы: </h3>
 * <li> add - сложение </li>
 * <li> sub - вычитание </li>
 * <li> multi - умножение </li>
 * <li> divi - целочисленное деление </li>
 * <li> divd - вещественное деление </li>
 * <li> remainder - остаток от деления </li>
 * <li> pow - возведение в указанную степень </li>
 *
 * @author D.I.Aleksandrovich
 *
 */

class Calc{

    /**
     * Метод возвращающий сумму a и b.
     *
     * @param a, b.
     * @return сумму a и b.
     */
    public static double add(double a, double b){
        return a + b;
    }

    /**
     * Метод возвращающий разность a и b.
     *
     * @param a, b.
     * @return разность a и b.
     */
    public static double sub(double a, double b){
        return a - b;
    }

    /**
     * Метод возвращающий произведение a на b.
     *
     * @param a, b.
     * @return произведение a на b.
     */
    public static double multi(double a, double b){
        return a * b;
    }

    /**
     * Метод возвращает целочисленное значение от целочисленного деления a на b.
     *
     * @param a, b.
     * @return целочисленное значение от целочисленного деления a на b.
     */
    public static int div(int a, int b){
        return a / b;
    }

    /**
     * Метод возвращает вещественное значение от вещественного деления a на b.
     *
     * @param a, b.
     * @return вещественное значение от вещественного деления a на b.
     */
    public static double div(double a, double b){
        return a / b;
    }

    /**
     * Метод вызвращающий остаток от деления a на b.
     *
     * @param a, b.
     * @return остаток от деления a на b.
     */
    public static double remainder(double a, double b){
        return (a % b);
    }

    /**
     * Метод возвращает 'a' в степени 'n'.
     *
     * @param x, n.
     * @return 'a' в степени 'n'.
     */
    public static double pow(double x, int n){
        double result = 1.0;

        for( ; n > 0; --n){
            result *= x;
        }

        return result;
    }

}