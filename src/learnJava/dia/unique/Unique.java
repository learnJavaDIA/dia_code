
package learnJava.dia.unique;


import java.util.HashSet;

public class Unique{

    public static void main(String[] args) {

        int[] srcArr = new int[16];
        randomInitArray(srcArr, -2, 2);

        System.out.println("Before:");
        printArray(srcArr);

        HashSet<Integer> numberHashSet = new HashSet<>();

        for(int i = 0; i < srcArr.length; ++i){

            numberHashSet.add(srcArr[i]);

        }

        System.out.println("After:");
        System.out.println(numberHashSet.toString());

    }

    private static void printArray(int array[]){

        System.out.print("[");

        int i = 0;

        while(i < (array.length - 1)){

            System.out.printf("%d, ", array[i]);
            ++i;

        }

        System.out.printf("%d]\n", array[i]);

    }

    private static void randomInitArray(int[] array, int min, int max){

        max -= min - 1;

        for(int i = 0; i < array.length; ++i){

            array[i] = min + (int)(Math.random() * max);

        }

    }

}