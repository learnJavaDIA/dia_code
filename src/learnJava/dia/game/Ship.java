
package learnJava.dia.game;


/**
 *
 * Класс описывающий кораблик.
 *
 * @author D.I.A
 */

class Ship{

    private int size;
    private int segments[];


    public Ship(int pos, int size){

        this.size = size;
        segments = new int[size];

        segments[0] = pos;
        for(int i = 1 ; i < size; ++i){
            segments[i] = pos + i;
        }
    }
    public Ship(){
        this(0, 3);
    }



    public void printSegments(){
        for (int i = 0; i < segments.length; ++i){
            System.out.print(segments[i] + " ; ");
        }
        System.out.println();
    }

    /**
     * Метод выстрела по текущему кораблику
     *
     * @param shotPos - позиция выстрела.
     * @return если есть попадание по кораблику возвращает true, иначе false.
     */
    public boolean thisShot(int shotPos){
        if(size > 0){
            for(int i = 0; i < segments.length ; ++i){
                if(segments[i] == shotPos){
                    segments[i] = -1;
                    --size;
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Метод определяет, потоплен кораблик или нет.
     *
     * @return в случае если кораблик потоплен, возвращает true, иначе false.
     */
    public boolean isSunk(){
        return (size <= 0);
    }


}