
package learnJava.dia.game;

import java.util.Scanner;
import learnJava.dia.game.Ship;

import java.util.Random;


/**
 * <h1> Класс игрового процесса </h1>
 *
 * <h3> Использует следующие методы </h3>
 * <li> public static void start(); </li>
 * <li> public static void attackShip(); </li>
 * <li> public static void printResult(); </li>
 *
 * @author D.I.A
 *
 */

public class Game{

    private static Scanner scan = new Scanner(System.in);

    private static Ship ship;

    private static int gotCount = 0;
    private static int slipCount = 0;

    private static final int START = 0;
    private static final int QUIT = -1;
    private static final int QUIT_TO_MENU = -2;

    private static final int MIN_FIELD = 0;
    private static final int MAX_FIELD = 12;

    private static final int MIN_SHIP_SIZE = 2;
    private static final int MAX_SHIP_SIZE = 4;

    private static final String MSG_SHOT = "Произведен выстрел в позицию : ";
    private static final String MSG_REQUEST_ACTION = "Введите : 0 для начала игры, -1 для выхода из игры.";
    private static final String MSG_REQUEST_SHOT_DATA = "Введите позицию выстрела или -2 для выходя в меню : ";
    private static final String MSG_IS_GOT = "Ты попал!";
    private static final String MSG_IS_NOT_GOT = "Ты не попал!";
    private static final String MSG_START_NEW_GAME = "Ты не попал!";
    private static final String MSG_UNKNOW_ACTION = "Неизвестная команда!";
    private static final String MSG_VICTORY = "Ты одержал победу!";

    public static void main(String[] args) {

        int action = 0;

        do{

            System.out.println(MSG_REQUEST_ACTION);
            action = scan.nextInt();


            if(action == START) {
                start();

                while (true){

                    System.out.println(MSG_REQUEST_SHOT_DATA);
                    action = scan.nextInt();

                    if(action == QUIT_TO_MENU){
                        break;

                    }else if(action >= MIN_FIELD && action < MAX_FIELD){

                        attackShip(action);

                    }else {
                        System.out.println(MSG_UNKNOW_ACTION);
                    }

                    if(ship.isSunk()){
                        System.out.println(MSG_VICTORY);
                        printResult();

                        break;
                    }

                }
            }


        }while (action != QUIT);

    }


    /**
     * Метод инициализации игровых данных.
     *
     */
    public static void start(){
        int shipSize = MIN_SHIP_SIZE + (int)(Math.random() * (MAX_SHIP_SIZE - MIN_SHIP_SIZE + 1));
        int shipPos = MIN_FIELD + (int)(Math.random() * (MAX_FIELD - shipSize + 1));

        ship = new Ship(shipPos, shipSize);

        gotCount = slipCount = 0;


        ship.printSegments();

    }

    /**
     * Метод производящий выстрел по кораблю.
     *
     * @param shotPos - позиция выстрела.
     */
    private static void attackShip(int shotPos){
        if(ship.thisShot(shotPos)){
            System.out.println(MSG_SHOT + shotPos + "\n" + MSG_IS_GOT);
            ++gotCount;
        }else{
            System.out.println(MSG_SHOT + shotPos + "\n" + MSG_IS_NOT_GOT);
            ++slipCount;
        }
    }

    /**
     * Метод вывода результата игры.
     *
     */
    private static void printResult(){
        System.out.println("\n****** Результат ******" +
                "\nПопаданий : " + gotCount +
                "\nПромахов : " + slipCount +
                "\nВсего выстрелов : " + (gotCount + slipCount) +
                "\n***********************" );
    }


}