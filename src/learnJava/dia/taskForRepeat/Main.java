
package learnJava.dia.taskForRepeat;

public class Main {

    public static void main(String[] args){

        System.out.printf("-5 * (-5) = %d\n", multi(-5, -5));
        System.out.printf("-9 * 6 = %d\n", multi(-9, 6));
        System.out.printf("7 * (-3) = %d\n", multi(7, -3));
        System.out.printf("8 * 8 = %d\n", multi(8, 8));
        System.out.printf("8 * 0 = %d\n", multi(8, 0));
        System.out.printf("0 * 8 = %d\n", multi(0, 8));

    }

    private static int multi(int a, int b){

        if(a == 0 || b == 0){

            return 0;

        }

        int result = 0;
        int sign = 1;

        if(a < 0){

            a = -a;
            sign = -sign;

        }
        if(b < 0){

            b = -b;
            sign = -sign;

        }

        if(a < b){

            result = b;
            for(int i = 1; i < a; ++i){

                result += b;

            }

        }else{

            result = a;
            for(int i = 1; i < b; ++i){

                result += a;

            }

        }

        return (sign < 0 ? -result : result);

    }

}
