
package learnJava.dia.file;

import java.io.*;
import java.util.Scanner;

public  class MainFile{

    private static Scanner scan = new Scanner(System.in);
    private static BufferedReader bufferedReader = null;
    private static BufferedWriter bufferedWriter = null;
    private static String fileName = null;

    private static final String MSG_REQUEST_ENTER_FILE_NAME = "enter file name: ";
    private static final String MSG_FILE_NOT_FIND = "File not find!";
    private static final String MSG_SELECT_ACTION = "Enter, %d - for print file contents\n" +
                                                    "%d - for add write in file\n" +
                                                    "%d - for quit to menu\n" +
                                                    "%d - for quit from program: ";

    private static final int CODE_QUIT = 100;
    private static final int CODE_PRINT_FILE_CONTENTS = 101;
    private static final int CODE_WRITE_IN_FILE = 102;
    private static final int CODE_QUIT_TO_MENU = 103;

    public static void main(String[] args) throws IOException {

        int actionCode = 0;

        do{

            System.out.println(MSG_REQUEST_ENTER_FILE_NAME);
            fileName = scan.nextLine();

            while (actionCode != CODE_QUIT && actionCode != CODE_QUIT_TO_MENU){

                System.out.printf(MSG_SELECT_ACTION, CODE_PRINT_FILE_CONTENTS, CODE_WRITE_IN_FILE, CODE_QUIT_TO_MENU, CODE_QUIT);
                actionCode = scan.nextInt();

                if(actionCode == CODE_PRINT_FILE_CONTENTS){
                    bufferedReader = new BufferedReader(new FileReader(fileName));
                    if(bufferedReader != null){
                        printFileContents();
                        bufferedReader.close();
                    }else {
                        System.out.println(MSG_FILE_NOT_FIND);
                    }

                }else if(actionCode == CODE_WRITE_IN_FILE){
                    bufferedWriter = new BufferedWriter(new FileWriter(fileName));
                    if(bufferedWriter != null){
                        System.out.print("enter new note: ");
                        String newStr = scan.nextLine();
                        addWriteInFile(newStr);
                        bufferedWriter.close();
                    }else {
                        System.out.println(MSG_FILE_NOT_FIND);
                    }
                }

            }

        }while (actionCode != CODE_QUIT);

        bufferedReader.close();
        bufferedWriter.close();

    }

    private static  void printFileContents() throws IOException{
        String str = null;
        while ((str = bufferedReader.readLine()) != null){
            System.out.println(str);
        }
    }

    private  static void addWriteInFile(String string) throws IOException{
        //bufferedWriter.newLine();
        bufferedWriter.write(string);
    }
}