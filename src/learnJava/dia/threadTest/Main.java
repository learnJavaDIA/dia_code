package dia.threadTest;

public class Main {

    public static void main(String[] args) {

        Thread thread = Thread.currentThread();

        System.out.println("Before thread: " + thread.toString());

        thread.setName("MainThreadDIA");
        thread.setPriority(10);

        System.out.println("After thread: " + thread.toString());

        for(int i = 0; i < 10; ++i){

            System.out.printf("thread (%d) (%s)\n", i,(new Thread()));

        }

    }

}
