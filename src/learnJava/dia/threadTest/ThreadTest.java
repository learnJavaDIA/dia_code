package dia.threadTest;

/**
 *
 * Class for learn threads
 *
 * @author D.I.A.
 *
 */

public class ThreadTest implements Runnable {

    public void run(){

        System.out.printf("thread: %s\n", Thread.currentThread());

    }

    public static void main(String[] args){

        for(int i = 5; i > 0; --i){

            (new Thread(new ThreadTest())).start();

        }

    }

}
