
package learnJava.dia.robot;

/**
 * Класс робота прямоходящего.
 *
 * <h3> Методы класса Robot: </h3>
 * <li> public void rotateToLeft(); </li>
 * <li> public void rotateToRight(); </li>
 * <li> public void stepForward(); </li>
 * <li> public void movement(Vector2 target); </li>
 * <li> public Vector2 getPosition(); </li>
 * <li> public Vector2 getDirection(); </li>
 *
 * @author D.I.A
 */

import learnJava.dia.Vector2D;

class Robot{
    private Vector2D position;
    private Vector2D direction;

    public Robot(Vector2D position, Vector2D direction){
        this.position = position;
        this.direction = direction;
    }

    public Robot(){
        this(new Vector2D(0, 0), new Vector2D(1, 0));
    }




    /**
     * Метод выполняющий смену направления относительно
     * текущего направления на 90 градусов вправо.
     *
     */
    public void rotateToRight(){
        int x = this.direction.x;
        int y = this.direction.y;
        this.direction.x = (x == 0) ? y : 0;
        this.direction.y = (y == 0) ? -x : 0;
        System.out.printf("Поворот направо на 90 градусов - dir(%d ; %d)\n", this.direction.x, this.direction.y);
    }

    /**
     * Метод выполняющий смену направления относительно
     * текущего направления на 90 градусов влево.
     *
     */
    public void rotateToLeft(){
        int x = this.direction.x;
        int y = this.direction.y;
        this.direction.x = (x == 0) ? -y : 0;
        this.direction.y = (y == 0) ? x : 0;
        System.out.printf("Поворот налево на 90 градусов - dir(%d ; %d)\n", this.direction.x, this.direction.y);
    }

    /**
     * Изменяет координаты позиции строго на 1ед. в зависимости от направления.
     *
     */
    public void stepForward(){
        this.position.add(this.direction);
        System.out.printf("Шаг вперед - pos(%d ; %d)\n", this.position.x, this.position.y);
    }

    /**
     * Выполняет пошаговое перемещение робота в заданную позицию.
     *
     * @param target - целевая позиция.
     */
    public void movement(Vector2D target){
        Vector2D deltaPos = new Vector2D(target.x - this.position.x, target.y - this.position.y);

        Vector2D directionToTarget = new Vector2D();

        directionToTarget.x = (deltaPos.x < 0) ? -1 : 1 ;

        //deltaPos.x  = (deltaPos.x < 0) ? -deltaPos.x : deltaPos.x;

        while(!this.direction.equals(directionToTarget)){
            this.rotateToRight();
        }

        while(this.position.x != deltaPos.x){
            this.stepForward();
        }

        directionToTarget.x = 0;
        directionToTarget.y = (deltaPos.y < 0) ? -1 : 1;

        //deltaPos.y = (deltaPos.y < 0) ? -deltaPos.y : deltaPos.y;

        while(!this.direction.equals(directionToTarget)){
            this.rotateToRight();
        }

        while(this.position.y != deltaPos.y){
            this.stepForward();
        }


    }

    public Vector2D getPosition() {
        return position;
    }

    public Vector2D getDirection() {
        return direction;
    }
}