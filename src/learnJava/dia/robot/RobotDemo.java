package learnJava.dia.robot;

/**
 * Класс демонстрирующий работу класса Robot.
 *
 * @author D.I.A
 */

import learnJava.dia.Vector2D;
import learnJava.dia.robot.Robot;

public class RobotDemo{
    public static void main(String[] args) {
        Robot robot = new Robot();


        Vector2D target = new Vector2D(-5, -7);
        robot.movement(target);


        System.out.println("Позиция - " + robot.getPosition());
        System.out.println("Направление - " + robot.getDirection());
    }
}