/**
 * Класс NumberPresent выполняет перевод введенных чисел из десятичной системы счисления
 * в двоичную и выводит результат в консоль.
 *
 *
 * @author D.I.A.
 */

//package learnJava.dia.presentNumber;

import java.util.Scanner;


public class NumberPresent{


    private static final String MSG_REQUEST = "Введите число которое хотите перевести,\nили exit чтобы выйти из программы: ";
    private static final String MSG_FORMAT_INTEGER = "Целое число %d,";
    private static final String MSG_FORMAT_DOUBLE = "Вещественное число %.2f,";
    private static final String MSG_FORMAT_BIN_PRESENT = " в двоичном представлении имеет вид %s";


    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        //System.out.print("Введите число которое хотите перевести: ");
        String numBuf = null;
        String binPresent = null;
        double doubleNum;
        long longBits;
        int intNum;
        boolean isQuit = false;

        do {

            System.out.print(MSG_REQUEST);
            numBuf = scan.nextLine();
            isQuit = numBuf.equalsIgnoreCase("exit");

            if(!isQuit){

                if (numBuf.contains(".")) {

                    doubleNum = Double.parseDouble(numBuf);
                    longBits = Double.doubleToLongBits(doubleNum);
                    binPresent = Long.toBinaryString(longBits);
                    binPresent = doubleNum > 0 ? "0" + binPresent : binPresent;

                    System.out.printf(MSG_FORMAT_DOUBLE + MSG_FORMAT_BIN_PRESENT, doubleNum, binPresent);
                    System.out.println();

                } else {

                    intNum = Integer.parseInt(numBuf);
                    binPresent = Integer.toBinaryString(intNum);

                    System.out.printf(MSG_FORMAT_INTEGER + MSG_FORMAT_BIN_PRESENT, intNum, binPresent);
                    System.out.println();

                }

            }


        }while (!isQuit);

    }

}