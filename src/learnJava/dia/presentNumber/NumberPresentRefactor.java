

package learnJava.dia.presentNumber;

import java.util.Scanner;

public class NumberPresentRefactor{


    private static final String MSG_FORMAT_DOUBLE_PRESENT = "Представление вещественного числа в формате чисел с плавающей точкой\nЧисло %10.15f: %s\n";
    private static final String MSG_FORMAT_INT_PRESENT    = "Разряды числа %d: %s\n";

    public static void main(String[] args) {
        /*
        int i1 = 389;
        String intBits1 = Integer.toBinaryString(i1);
        System.out.println("Разряды числа: " + intBits1);

        int i2 = -386;
        String intBits2 = Integer.toBinaryString(i2);
        System.out.println("Разряды числа: " + intBits2);

        double d1 = 75.38;
        String sResult1 = "";
        long numberBits1 = Double.doubleToLongBits(d1);

        sResult1 = Long.toBinaryString(numberBits1);
        System.out.println("Представление вещественного числа в формате чисел с плавающей точкой");

        System.out.format("Число: %5.2f\n", d1);
        System.out.println("Формат чисел с плавающей точкой:");

        System.out.println(d1 > 0 ? "0" + sResult1 : sResult1);

        double d2 = Math.PI;
        String sResult2 = "";
        long numberBits2 = Double.doubleToLongBits(d2);

        sResult2 = Long.toBinaryString(numberBits2);
        System.out.println("Представление вещественного числа в формате чисел с плавающей точкой");

        System.out.format("Число: %10.15f\n", d2);
        System.out.println("Формат чисел с плавающей точкой:");

        System.out.println(d2 > 0 ? "0" + sResult2 : sResult2);
        */

        Scanner scan = new Scanner(System.in);

        String strNumberBuf = scan.nextLine();

        if(strNumberBuf.contains(".")){

            double doubleNumber = Double.parseDouble(strNumberBuf);
            System.out.printf(MSG_FORMAT_DOUBLE_PRESENT, doubleNumber, getBinaryString(doubleNumber));

        }else{

            int intNumber = Integer.parseInt(strNumberBuf);
            System.out.printf(MSG_FORMAT_INT_PRESENT, intNumber, getBinaryString(intNumber));

        }


    }

    private static String getBinaryString(int number){
        return Integer.toBinaryString(number);
    }

    private static String getBinaryString(double number){
        //long longBits = Double.doubleToLongBits(number);
        return ((number > 0 ? "0" + Long.toBinaryString(Double.doubleToLongBits(number)) : Long.toBinaryString(Double.doubleToLongBits(number))));
    }

}