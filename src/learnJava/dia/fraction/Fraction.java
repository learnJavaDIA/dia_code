
package learnJava.dia.fraction;


public class Fraction{

    private int numerator;
    private int denominator;


    public Fraction(){
        numerator = 0;
        denominator = 1;
    }

    public Fraction(int numerator, int denominator){

        if(denominator < 0){

            this.numerator = -numerator;
            this.denominator = -denominator;

        }else{

            this.numerator = numerator;
            this.denominator = denominator;

        }

    }

    public Fraction(int whole, int numerator, int denominator){
        this(numerator + whole * denominator, denominator);
    }


    public int getNumerator(){
        return numerator;
    }

    public int getDenominator(){
        return denominator;
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    @Override
    public String toString() {

        String result;

        if(numerator >= denominator){

            if((numerator % denominator) == 0){

                return Integer.toString(numerator / denominator);

            }else{

                return ((numerator / denominator) + "(" + (numerator % denominator) + "/" + denominator + ")");

            }

        }

        return (numerator + "/" + denominator);
    }

    /**
     * Метод преобразующий дробное значение представленное в виде строки в дробь типа Fraction.
     *
     * @param strFraction
     * @return дробь типа Fraction в случае если значение корректно, иначе - null.
     */
    public static Fraction parseFraction(String strFraction){

        String fractionParts[] = strFraction.split("[^\\d-+]");

        if(fractionParts.length == 2 && fractionParts[0].length() > 0 && fractionParts[1].length() > 0){
            return new Fraction(Integer.parseInt(fractionParts[0]), Integer.parseInt(fractionParts[1]));

        }else if(fractionParts.length == 3 && fractionParts[0].length() > 0 && fractionParts[1].length() > 0 && fractionParts[2].length() > 0){
            return new Fraction(Integer.parseInt(fractionParts[0]), Integer.parseInt(fractionParts[1]), Integer.parseInt(fractionParts[2]));
        }

        return null;

    }

    /**
     * Метод нахождения наименьшего общего кратного двух чисел,
     * denominator1'a и denominator2'a.
     * @param denominator1
     * @param denominator2
     * @return наименьшее общее кратное.
     */
    private  static int findCommonDenomirator(int denominator1, int denominator2){

        int greatestDenominator = denominator1 > denominator2 ? denominator1 : denominator2;
        int leastDenominator    = denominator1 < denominator2 ? denominator1 : denominator2;
        int commonDenominator = greatestDenominator;

        while ((commonDenominator % leastDenominator) != 0){

            commonDenominator += greatestDenominator;

        }

        return commonDenominator;

    }

    /**
     * Метод приведения к общему знаменателю двух дробей,
     * fraction1 и fraction2.
     * @param fraction1
     * @param fraction2
     */
    public static void coercionToCommonDenominator(Fraction fraction1, Fraction fraction2){

        int commonDenominator = findCommonDenomirator(fraction1.getDenominator(), fraction2.getDenominator());

        fraction1.setNumerator(fraction1.getNumerator() * (commonDenominator / fraction1.getDenominator()));
        fraction1.setDenominator(commonDenominator);

        fraction2.setNumerator(fraction2.getNumerator() * (commonDenominator / fraction2.getDenominator()));
        fraction2.setDenominator(commonDenominator);

    }

    /**
     * Метод сокращения дроби fraction.
     * @param fraction
     */
    public static void cut(Fraction fraction){

        for(int i = fraction.getDenominator() < fraction.getNumerator() ? fraction.getDenominator() : fraction.getNumerator(); i > 0; --i){

            if((fraction.getNumerator() % i == 0) && (fraction.getDenominator() % i == 0)){

                fraction.setNumerator(fraction.getNumerator() / i);
                fraction.setDenominator(fraction.getDenominator() / i);

                break;
            }

        }

    }

    /**
     * Метод сокращает данную дробь.
     */
    public void cut(){

        for(int i = denominator < numerator ? denominator : numerator; i > 0; --i){

            if((numerator % i == 0) && (denominator % i == 0)){

                numerator /= i;
                denominator /= i;

                break;
            }

        }

    }

    /**
     * Метод нахождения суммы дробей fraction1 и fraction2.
     * @param fraction1
     * @param fraction2
     * @return дробь суммы fraction1 и fraction2.
     */
    public static Fraction add(Fraction fraction1, Fraction fraction2){

        if(fraction1.getDenominator() != fraction2.getDenominator()){

            coercionToCommonDenominator(fraction1, fraction2);

        }

        Fraction result = new Fraction(fraction1.getNumerator() + fraction2.getNumerator(), fraction1.getDenominator());
        result.cut();

        return result;

    }

    /**
     * Метод нахождения разности дробей fraction1 и fraction2.
     * @param fraction1
     * @param fraction2
     * @return дробь разности fraction1 и fraction2.
     */
    public static Fraction sub(Fraction fraction1, Fraction fraction2){

        if(fraction1.getDenominator() != fraction2.getDenominator()){

            coercionToCommonDenominator(fraction1, fraction2);

        }

        Fraction result =  new Fraction(fraction1.getNumerator() - fraction2.getNumerator(), fraction1.getDenominator());
        result.cut();

        return result;

    }

    /**
     * Метод нахождения произведения дробей fraction1 и fraction2.
     * @param fraction1
     * @param fraction2
     * @return дробь произведения fraction1 и fraction2.
     */
    public static Fraction multi(Fraction fraction1, Fraction fraction2){

        Fraction result = new Fraction(fraction1.getNumerator() * fraction2.getNumerator(), fraction1.getDenominator() * fraction2.getDenominator());
        result.cut();

        return result;

    }

    /**
     * Метод нахождения частного дробей  fraction1 и fraction2.
     * @param fraction1
     * @param fraction2
     * @return дробь частного fraction1 и fraction2.
     */
    public static Fraction div(Fraction fraction1, Fraction fraction2){

        if(fraction1.getNumerator() == 0){

            return new Fraction();

        }

        Fraction result = new Fraction(fraction1.getNumerator() * fraction2.getDenominator(), fraction1.getDenominator() * fraction2.getNumerator());
        result.cut();

        return result;

    }



}