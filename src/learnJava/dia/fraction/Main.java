
package learnJava.dia.fraction;


import learnJava.dia.fraction.Fraction;

import java.io.*;

public class Main{


    private static final String MSG_ERROR = "error";
    private static final String MSG_PROGRAM_COMPLETE = "Выполнено!";

    private static final String PATH_INPUT_FILE  = "C:\\Users\\DatLik\\IdeaProjects\\DIA_Code\\src\\learnJava\\dia\\fraction\\input.txt";
    private static final String PATH_OUTPUT_FILE = "C:\\Users\\DatLik\\IdeaProjects\\DIA_Code\\src\\learnJava\\dia\\fraction\\output.txt";


    public static void main(String[] args) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new FileReader(PATH_INPUT_FILE));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(PATH_OUTPUT_FILE));


        String expression = bufferedReader.readLine();
        String result = null;

        String expParts[] = expression.split(" ");

        if(expParts.length == 3){

            Fraction fraction1 = Fraction.parseFraction(expParts[0]);
            Fraction fraction2 = Fraction.parseFraction(expParts[2]);

            if(fraction1 != null && fraction1.getDenominator() != 0 && fraction2 != null && fraction2.getDenominator() != 0){

                Fraction fractionResult = null;

                switch (expParts[1]){

                    case "+":
                        fractionResult = Fraction.add(fraction1, fraction2);
                        break;
                    case "-":
                        fractionResult = Fraction.sub(fraction1, fraction2);
                        break;
                    case "/":
                        fractionResult = Fraction.div(fraction1, fraction2);
                        break;
                    case "*":
                        fractionResult = Fraction.multi(fraction1, fraction2);
                        break;

                }

                if(fractionResult != null){

                    bufferedWriter.write(expression + " = " + fractionResult.toString());

                }else {

                    System.out.println(MSG_ERROR);
                    bufferedWriter.write(MSG_ERROR);

                }

            }else{

                System.out.println(MSG_ERROR);
                bufferedWriter.write(MSG_ERROR);

            }

        }else{

            System.out.println(MSG_ERROR);
            bufferedWriter.write(MSG_ERROR);

        }


        bufferedReader.close();
        bufferedWriter.close();


        System.out.println(MSG_PROGRAM_COMPLETE);


    }

}