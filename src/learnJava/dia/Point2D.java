
package learnJava.dia;


public class Point2D {
    public double x, y;

    public Point2D(double x, double y){
        this.x = x;
        this.y = y;
    }
    public Point2D(){
        x = y = 0;
    }

    public String toString(){
        return String.format("( %.2f ; %.2f )", (float)x, (float)y);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public boolean isEqual(Point2D point){
        return (this.x == point.x && this.y == point.y);
    }

}