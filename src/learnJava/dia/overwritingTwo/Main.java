
package learnJava.dia.overwritingTwo;


import java.io.IOException;


public class Main{

    private static final int AMOUNT_NUMBERS = 100000;
    private static final int MIN_NUMBER = 10;
    private static final int MAX_NUMBER = 9999989;


    private static final String FILE_PATH_NUMBERS_TXT = "C:\\Users\\DatLik\\IdeaProjects\\DIA_Code\\src\\learnJava\\dia\\overwriting\\numbers.txt";
    private static final String FILE_PATH_NUMBERS_DAT = "C:\\Users\\DatLik\\IdeaProjects\\DIA_Code\\src\\learnJava\\dia\\overwriting\\numbers.dat";
    private static final String FILE_PATH_NUMBERS6_DAT = "C:\\Users\\DatLik\\IdeaProjects\\DIA_Code\\src\\learnJava\\dia\\overwriting\\numbers6.dat";
    private static final String FILE_PATH_LUCKYNUMBERS_TXT = "C:\\Users\\DatLik\\IdeaProjects\\DIA_Code\\src\\learnJava\\dia\\overwriting\\luckyNumbers.txt";

    public static void main(String[] args) throws IOException {
/*
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(FILE_PATH_NUMBERS_TXT));

        for(int i = 0; i < AMOUNT_NUMBERS; ++i){
            bufferedWriter.write(Integer.toString(MIN_NUMBER + (int)(Math.random() * MAX_NUMBER)));
            bufferedWriter.newLine();
        }
*/

        RewriteTxtToInt rewriteTxtToInt = new RewriteTxtToInt(FILE_PATH_NUMBERS_TXT, FILE_PATH_NUMBERS_DAT);
        rewriteTxtToInt.rewrite();
        rewriteTxtToInt.close();

        RewriteIntToInt rewriteIntToInt = new RewriteIntToInt(FILE_PATH_NUMBERS_DAT, FILE_PATH_NUMBERS6_DAT);
        rewriteIntToInt.filterRewrite(1001, 999999);
        rewriteIntToInt.close();

        RewriteIntToLuckyTxt rewriteIntToLuckyTxt = new RewriteIntToLuckyTxt(FILE_PATH_NUMBERS6_DAT, FILE_PATH_LUCKYNUMBERS_TXT);
        rewriteIntToLuckyTxt.rewrite();
        rewriteIntToLuckyTxt.close();

    }

}