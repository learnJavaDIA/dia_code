
package learnJava.dia.interfaces;


public interface Detect{
    boolean isDetectObstacle();
    boolean isDetectItem();
}