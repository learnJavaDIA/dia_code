
package learnJava.dia.interfaces;

public interface Action{
    void toTake();
    void toThrow();
}