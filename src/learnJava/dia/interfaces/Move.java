
package learnJava.dia.interfaces;

public interface Move{
    void toForward();
    void toRotate();
}