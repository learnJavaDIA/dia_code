
package learnJava.dia.calc2;

/**
 * Улучшенная версия калькулятора.
 *
 * Вычисляет значение введенного в строчку выражения, но пока допустимы только выражение из 2х
 * операндов.
 *
 * @author D.I.Aleksandrovich
 *
 */

import learnJava.dia.calc2.Calc;
import java.util.Scanner;

class CalcDemo{

    private static Scanner scan = new Scanner(System.in);

    // Массивы содержащие допустимы в выражении символы.
    private static char[] allowableChars = {'.', ',', '(', ')','e', 'E'};
    private static char[] allowableOperators = {'+', '-', '*', '/', '^', '%'};

    // Именованные строковые константы представляющие сообщения.
    private static final String ERR_MSG_INVALID_CHARACTER = "Выражение содержит недопустимый символ!";
    private static final String ERR_MSG_NONE_OPERATORS = "Выражение не содержит операторов!";
    private static final String ERR_MSG_DIVISION_BY_ZERO = "Выражение содержит деление на ноль!";
    private static final String ERR_MSG_UNACCEPTABLE_OPERATOR = "Недопустимый оператор!";


    public static void main(String[] args){

        System.out.print("Введите выражение: ");
        String exp = scan.nextLine();


        double result = calculationExpressionFromString_2(exp);

        System.out.println("Ответ: " + result);

    }

    /**
     * Методу для корректного вычисления требуется передать выражение в виде
     * одной строковой константы из двух операндов и
     * знака операции между ними, также перед операндами возможен унарный оператор '-' или '+'.
     *
     * @param expression
     * @return возвращает результат вычисления арифметического выражения
     * представленного в виде строковой константы expression, вещественного типа.
     */
    private static double calculationExpressionFromString_2(String expression){
        String[] expChars = expression.split("");

        String operator = "\0";
        String leftOperand = "\0";
        String rightOperand = "\0";

        for(int i = 0; i < expChars.length; ++i){

            if(expChars[i].charAt(0) != ' '){
                if(isOperator(expChars[i].charAt(0))){
                    if(leftOperand == "\0"){
                        leftOperand += expChars[i];
                    }
                    else  if(operator != "\0" && rightOperand == "\0"){
                        rightOperand += expChars[i];
                    }else{
                        operator = expChars[i];
                    }
                }else {
                    if (operator == "\0") {
                        leftOperand += expChars[i];
                    } else {
                        rightOperand += expChars[i];
                    }
                }
            }

        }

        return calculation(Double.parseDouble(leftOperand), Double.parseDouble(rightOperand), operator.charAt(0));
    }

    /**
     * Метод вычисляющий значение выражения представленного в виде строкового объекта.
     *
     * @param expression
     * @return double
     */
    private static double calculationExpressionFromString(String expression){
        double result = 0.0;

        if(!expressionSyntaxAnalysis(expression)){
            System.out.println(ERR_MSG_INVALID_CHARACTER);
            return -1.0;
        }else {

            double leftOperand = 0.0;
            double rightOperand = 0.0;

            char[] expressionChars = expression.toCharArray();
            char operator = getOperatorAndSetSpace(expressionChars);
            expression = new String(expressionChars);

            if(operator == '\0'){
                System.out.println(ERR_MSG_NONE_OPERATORS);
            }else {

                String[] operands = expression.split(" ", 2);

                leftOperand = Double.parseDouble(operands[0]);
                rightOperand = Double.parseDouble(operands[1]);

                result = calculation(leftOperand, rightOperand, operator);
            }

        }

        return result;
    }

    /**
     * Метод выполняет цказанную операцию operator над операндами leftOperand и rightOperand.
     *
     * @param leftOperand
     * @param rightOperand
     * @param operator
     * @return double
     */
    private static double calculation(double leftOperand, double rightOperand, char operator){
        switch (operator){
            case '+':
                return Calc.add(leftOperand, rightOperand);
            case '-':
                return Calc.sub(leftOperand, rightOperand);
            case '*':
                return Calc.multi(leftOperand, rightOperand);
            case '/':
                if(rightOperand == 0){
                    System.out.println(ERR_MSG_DIVISION_BY_ZERO);
                    return -1.0;
                }else{
                    if(leftOperand % 1.0 == 0 && rightOperand % 1.0 == 0){
                        return Calc.div((int)leftOperand, (int)rightOperand);
                    }else{
                        return Calc.div(leftOperand, rightOperand);
                    }
                }
            case '%':
                if(rightOperand == 0){
                    System.out.println(ERR_MSG_DIVISION_BY_ZERO);
                    return -1.0;
                }else{
                    return Calc.remainder(leftOperand, rightOperand);
                }
            case '^':
                return Calc.pow(leftOperand, (int) rightOperand);
            default:
                System.out.println(ERR_MSG_UNACCEPTABLE_OPERATOR);
                return -1.0;
        }
    }

    /**
     * Метод выполняет проверку строкового выражения на недопустимые символы,
     * если все символы допустимы, то возвращается true, иначе false.
     *
     * @param expression
     * @return boolean
     */
    private static boolean expressionSyntaxAnalysis(String expression){
        int expStrLen = expression.length();
        int openingBracketCount = 0;
        int closingBracketCount = 0;
        char symbol;

        for(int i = 0; i < expStrLen; ++i){
            symbol = expression.charAt(i);

            // Проверка на допустимость символа в выражении.
            if(!isDigit(symbol) && !isOtherAllowableChar(symbol) && !isOperator(symbol)){
                return false;
            }

            // begin
            // Блок анализа правильности ввода скобок в выражении.
            if(symbol == ')' && openingBracketCount == 0){
                return  false;
            }

            if(symbol == '('){
                ++openingBracketCount;
            }
            if(symbol == ')'){
                ++closingBracketCount;
            }
            // end.
        }

        System.out.println(openingBracketCount);
        System.out.println(closingBracketCount);

        return (openingBracketCount == closingBracketCount);
    }

    /**
     * Метод возвращает символ оператора если он есть в строке, иначе возвращается
     * нулевой символ.
     *
     * @param expression
     * @return char
     */
    private static char getOperator(String expression){
        int expStrLen = expression.length();

        for(int i = 0; i < expStrLen; ++i){
            if(isOperator(expression.charAt(i))){
                return expression.charAt(i);
            }
        }

        return '\0';
    }

    /**
     * Метод возвращает оператор если он есть в массиве символов и на его место
     * вставляет символ пробела, иначе возвращает нулевой символ.
     *
     * @param array
     * @return char
     */
    private static char getOperatorAndSetSpace(char[] array){
        char symbol;

        for(int i = 0; i < array.length; ++i){
            symbol = array[i];
            if(isOperator(symbol)){
                array[i] = ' ';
                return symbol;
            }
        }

        return '\0';
    }

    /**
     * Метод возвращающий true если символ 'c' является символом цифры, иначе false.
     *
     * @param c
     * @return boolean
     */
    private static boolean isDigit(char c){
        return (c >= '0' || c <= '9');
    }

    /**
     * Метод возвращает true если символ 'c' является символом из массива
     * допустимых операторов allowableOperators, иначе false.
     *
     * @param c
     * @return boolean
     */
    private static boolean isOperator(char c){
        for(int i = 0; i < allowableOperators.length; ++i){
            if(c == allowableOperators[i]){
                return true;
            }
        }

        return false;
    }

    /**
     * Метод возвращает true если символ c является символом из массива
     * других допустимых символов allowableChars, иначе false.
     *
     * @param c
     * @return boolean
     */
    private static boolean isOtherAllowableChar(char c){
        for(int i = 0; i < allowableChars.length; ++i){
            if(c == allowableChars[i]){
                return true;
            }
        }

        return false;
    }
}