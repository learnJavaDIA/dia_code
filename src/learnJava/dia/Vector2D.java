
package learnJava.dia;


public class Vector2D{
    public int x, y;


    public Vector2D(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Vector2D(){
        this(0, 0);
    }

    public String toString(){
        return "(" + this.x + " ; " + this.y + ")";
    }

    /**
     * Метод суммирует текущий и vec2 вектор.
     *
     * @param vec
     */
    public void add(Vector2D vec){
        this.x += vec.x;
        this.y += vec.y;
    }

    /**
     * Метод вычитает из текущиго вектора vec2 вектор.
     *
     * @param vec
     */
    public void sub(Vector2D vec){
        this.x -= vec.x;
        this.y -= vec.y;
    }

    /**
     * Метод умножает текущий вектор на число 'a'.
     *
     * @param a
     */
    public void multi(int a){
        this.x *= a;
        this.y *= a;
    }

    /**
     * Метод сравнивает соответствующие координаты
     * текущего вектора с vec2 координатами вектора.
     *
     * @param vec
     * @return true если координаты равны, иначе false.
     */
    public boolean equals(Vector2D vec){
        return (this.x == vec.x && this.y == vec.y);
    }

    /**
     * Метод складывает вектор 'vec1' с вектором 'vec2'.
     *
     * @param vec1
     * @param vec2
     * @return сумму векторов 'vec1' и 'vec2'.
     */
    public static Vector2D add(Vector2D vec1, Vector2D vec2){
        return new Vector2D(vec1.x + vec2.x, vec1.y + vec2.y);
    }

    /**
     * Метод вычитает из вектора 'vec1' вектор 'vec2'.
     *
     * @param vec1
     * @param vec2
     * @return разность векторов 'vec1' и 'vec2'.
     */
    public static Vector2D sub(Vector2D vec1, Vector2D vec2){
        return new Vector2D(vec1.x - vec2.x, vec1.y - vec2.y);
    }

    /**
     * Метод умножает вектор 'vec1' на число 'a'.
     *
     * @param vec
     * @param a
     * @return произведение вектора 'vec' на число 'a'.
     */
    public static Vector2D multi(Vector2D vec, int a){
        return new Vector2D(vec.x * a, vec.y * a);
    }
}