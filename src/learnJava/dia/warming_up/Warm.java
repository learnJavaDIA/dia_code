
package learnJava.dia.warming_up;



public class Warm{

    public static void main(String[] args){

        int[] arr1 = {23, 34, 55, 12, 0, 12, 33, 12, 11};
        int t1 = 6;

        int[] arr2 = {7, 34};
        int t2 = 8;

        int[] arr3 = {4, 3, 9, 2, 1, 5, 2};
        int t3 = 3;

        bubbleSortMaxToMin(arr1);
        bubbleSortMaxToMin(arr2);
        bubbleSortMaxToMin(arr3);

        System.out.printf("time = %d ; cost = ", t1);
        printArray(arr1);
        System.out.printf("money 1 = %d\n",money(t1, arr1));

        System.out.printf("time = %d ; cost = ", t2);
        printArray(arr2);
        System.out.printf("money 2 = %d\n",money(t2, arr2));

        System.out.printf("time = %d ; cost = ", t3);
        printArray(arr3);
        System.out.printf("money 3 = %d\n",money(t3, arr3));

    }

    private static int money(int time, int[] cost){

        int result = 0;

        for(int i = 0; i < cost.length && time != 0; ++i, --time){
            result += cost[i];
        }

        return result;

    }

    private static void printArray(int[] array){

        System.out.print('[');
        for(int i = 0; i < array.length; ++i){

            System.out.printf("%d ,",array[i]);

        }
        System.out.println(']');

    }

    /**
     * Пузырьковая сортировка массива, расставляет элементы от наибольшего к наименьшему.
     *
     * @param arr - сортируемый массив.
     */
    private static void bubbleSortMaxToMin(int[] arr){

        int b = 0;

        for(int e = arr.length - 1; e > 0; --e){

            for(int i = 0; i < e; ++i){

                if(arr[i] < arr[i + 1]){
                    b = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = b;
                }

            }

        }

    }
}