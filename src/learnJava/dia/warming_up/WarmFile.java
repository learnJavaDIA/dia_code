
package learnJava.dia.warming_up;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class WarmFile{

    private static final String FILE_PATH = "C:\\Users\\DatLik\\IdeaProjects\\DIA_Code\\src\\learnJava\\dia\\warming_up\\cost.txt";

    public static void main(String[] args) throws IOException{

        BufferedReader bufferedReader = new BufferedReader(new FileReader(FILE_PATH));

        String buf = null;

        while ((buf = bufferedReader.readLine()) != null){

            String [] arrBuf = buf.split(" ");

            int time = Integer.parseInt(arrBuf[0]);
            arrBuf[0] = null;

            int[] cost = new int[arrBuf.length - 1];

            for(int i = 0, q = 0; q < arrBuf.length; ++q){

                if(arrBuf[q] != null){
                    cost[i] = Integer.parseInt(arrBuf[q]);
                    ++i;
                }
            }

            bubbleSortMaxToMin(cost);

            System.out.printf("time = %d ; cost = ", time);
            printArray(cost);
            System.out.printf("money = %d\n", money(time, cost));


        }

        bufferedReader.close();

    }

    private static int money(int time, int[] cost){

        int result = 0;

        for(int i = 0; i < cost.length && time != 0; ++i, --time){
            result += cost[i];
        }

        return result;

    }

    private static void printArray(int[] array){

        System.out.print('[');
        for(int i = 0; i < array.length; ++i){

            System.out.printf("%d ,",array[i]);

        }
        System.out.println(']');

    }

    /**
     * Пузырьковая сортировка массива, расставляет элементы от наибольшего к наименьшему.
     *
     * @param arr - сортируемый массив.
     */
    private static void bubbleSortMaxToMin(int[] arr){

        int b = 0;

        for(int e = arr.length - 1; e > 0; --e){

            for(int i = 0; i < e; ++i){

                if(arr[i] < arr[i + 1]){
                    b = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = b;
                }

            }

        }

    }
}