
package learnJava.dia.boxHierarchy;

import learnJava.dia.boxHierarchy.ColorBox;

class HeavyBox extends ColorBox{
    private double mass;

    public HeavyBox(double width, double height, double deep, double mass, Color color){
        super(width, height, deep, color);
        this.mass = mass;
    }

    public HeavyBox(){
        this(1, 1, 1, 0, Color.BLACK);
    }

    public String toString(){
        return String.format(super.toString() + "Mass %.2f\n", (float) mass);
    }

    public double getMass() {
        return mass;
    }
}