
package learnJava.dia.boxHierarchy;

import learnJava.dia.boxHierarchy.HeavyBox;


public class BoxMain{

    public static void main(String[] args) {

        Box boxes[] = {
                new HeavyBox(18, 23,34, 256, Color.RED),
                new HeavyBox(23, 43,13, 22, Color.RED),
                new HeavyBox(13, 12,23, 45, Color.GREEN),
                new HeavyBox(32, 24,16, 12, Color.GREEN),
                new HeavyBox(14, 33,11, 3, Color.RED),
                new HeavyBox(21, 16,17, 24, Color.GREEN)
        };

        Box result = searchBox(boxes, "capacity", Color.GREEN, true);

    }

    private static Box searchBox(Box[] boxes, String param,Color color,boolean isMore){
        Box result = boxes[0];

        if(param.toLowerCase() == "capacity"){
            double bufCapacity = 0;

            if(isMore){
                for(int i = 1; i < boxes.length; ++i){
                    if(result.getCapacity() < boxes[i].getCapacity()){
                        result = boxes[i];
                    }
                }
            }else {
                for(int i = 1; i < boxes.length; ++i){
                    if(result.getCapacity() > boxes[i].getCapacity()){
                        result = boxes[i];
                    }
                }
            }

        }else{
            double bufMass = 0;

            if(isMore){
                for(int i = 1; i < boxes.length; ++i){
                    if(((HeavyBox)result).getMass() < ((HeavyBox)boxes[i]).getMass()){
                        result = boxes[i];
                    }
                }
            }else {
                for(int i = 1; i < boxes.length; ++i){
                    if(((HeavyBox)result).getMass() > ((HeavyBox)boxes[i]).getMass()){
                        result = boxes[i];
                    }
                }
            }

        }

        return result;

    }

}