
package learnJava.dia.boxHierarchy;

class Box{
    private double width, height, deep;

    public Box(double width, double height, double deep){
        this.width = width;
        this.height = height;
        this.deep = deep;
    }

    public Box(){
        this(1, 1, 1);
    }

    public String toString(){
        return String.format("Scale (%.2f ; %.2f ; %.2f)\n", (float)width, (float)height, (float)deep);
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double getDeep() {
        return deep;
    }


    public double getCapacity(){
        return (width * height * deep);
    }


}