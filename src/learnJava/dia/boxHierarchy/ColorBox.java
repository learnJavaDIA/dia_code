
package learnJava.dia.boxHierarchy;

import learnJava.dia.boxHierarchy.Box;

class ColorBox extends Box{
    private Color color;

    public ColorBox(double width, double height, double deep, Color color){
        super(width, height, deep);
        this.color = color;
    }

    public ColorBox(){
        this(1, 1, 1, Color.BLACK);
    }

    public String toString(){
        return String.format(super.toString() + "Color %s\n", color.toString());
    }

    public Color getColor() {
        return color;
    }

}