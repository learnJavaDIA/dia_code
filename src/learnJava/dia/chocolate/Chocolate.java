/**
 * Класс Chocolate подсчитывает, какое количество шоколадок возможно получить,
 * при имеющихся: средствах - money, цене - price и "акции" (т.е. количество оберток
 * которые можно обменять на одну шоколадку) - stock.
 *
 *
 * @author D.I.A.
 */

package learnJava.dia.chocolate;

import java.util.Scanner;


public class Chocolate{


    private static final String MSG_GET_MONEY = "Введите количество имеющихся у вас денег: ";
    private static final String MSG_GET_PRICE = "Введите цену одной шоколадки: ";
    private static final String MSG_GET_STOCK = "Введите количество оберток за которое можно получить еще одну шоколадку: ";
    private static final String MSG_FORMAT_ONLY_CHOCOLATES = "Всего получите шоколадок - %d шт.\n";


    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        int money      = 0;
        int price      = 0;
        int stock      = 0;
        int chocolates = 0;
        int wrap       = 0;


        System.out.print(MSG_GET_MONEY);
        money = scan.nextInt();

        System.out.print(MSG_GET_PRICE);
        price = scan.nextInt();

        System.out.print(MSG_GET_STOCK);
        stock = scan.nextInt();

        wrap = chocolates = money / price;


        while (wrap >= stock){

            wrap -= stock;
            ++chocolates;
            ++wrap;

        }

        //for( ; wrap >= stock; wrap -= stock, ++chocolates, ++wrap);

        /*
        while (wrap >= stock){

            chocolates += wrap / stock;
            wrap += (wrap / stock) - (wrap / stock) * stock;

        }*/


        System.out.printf(MSG_FORMAT_ONLY_CHOCOLATES, chocolates);

    }

}