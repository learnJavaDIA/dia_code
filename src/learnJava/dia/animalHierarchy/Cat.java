
package learnJava.dia.animalHierarchy;

import learnJava.dia.animalHierarchy.Animal;

/**
 * Класс сурового кота Бориса, наследующий функциональность абстрактного вида животных Animal.
 *
 * @author D.I.A
 */

class Cat extends Animal{

    public Cat(String name, int maxEnergy, int minEnergy, int activity){
        super(name, maxEnergy, minEnergy, activity);
    }
    public Cat(){
        super();
    }

    public String toString(){
        return super.toString();
    }


    /**
     * Жрущий котяра.
     */
    public void eat(){
        super.currentEnergy += super.maxEnergy * 8 / 100;
    }
    /**
     * Играющий котяра.
     */
    public void play(){
        super.currentEnergy -= super.currentEnergy * 10 / 100 * super.activity;
    }
    /**
     * Спящий котяра.
     */
    public void sleep(){
        super.currentEnergy += super.maxEnergy * 5 / 100;
    }

    /**
     * Говорящий котяра.
     */
    public void talk(){
        System.out.println("Mew-mew-meeeeeeeeewww...");
    }
}