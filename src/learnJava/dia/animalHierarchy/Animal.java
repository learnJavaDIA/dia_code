
package learnJava.dia.animalHierarchy;

/**
 * Абстрактный класс, в котором выделены общие поля и интерфейс.
 *
 * @author D.I.A
 */

abstract class Animal{
    public String name;
    public int maxEnergy;
    public int minEnergy;
    public int currentEnergy;
    public int activity;

    public Animal(String name, int maxEnergy, int minEnergy, int activity){
        this.name = name;
        this.currentEnergy = this.maxEnergy = maxEnergy;
        this.minEnergy = minEnergy;
        this.activity = activity;
    }
    public Animal(){
        this("Unknow", 0, 0, 0);
    }

    public String toString(){
        return String.format(
                "Name : %s\n" +
                "Max Energy : %s\n" +
                "Current Energy : %s\n" +
                "Activity : %s\n",

                name, maxEnergy, currentEnergy, activity
        );
    }

    /**
     * Метод определения голода животного.
     *
     * @return возвращает true если текущий уровень энергии не меньше минимально допустимого,
     * иначе false.
     */
    public boolean isHungry(){
        return currentEnergy < minEnergy;
    }

    /**
     * Кормление животного.
     */
    abstract public void eat();

    /**
     * Животное играет.
     */
    abstract public void play();

    /**
     * Спящее животное.
     */
    abstract public void sleep();

    /**
     * Животное хочет что-то сказать...
     */
    abstract public void talk();


}