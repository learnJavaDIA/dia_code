
package learnJava.dia.animalHierarchy;

import learnJava.dia.animalHierarchy.Animal;

/**
 * Класс собаки, наследующий функциональность абстрактного вида животных Animal.
 *
 * @author D.I.A
 */

class Dog extends Animal{

    public Dog(String name, int maxEnergy, int minEnergy, int activity){
        super(name, maxEnergy, minEnergy, activity);
    }
    public Dog(){
        super();
    }

    public String toString(){
        return super.toString();
    }


    /**
     * Жрущий собакен.
     */
    public void eat(){
        super.currentEnergy += super.maxEnergy * 8 / 100;
    }
    /**
     * Играющий собакен.
     */
    public void play(){
        super.currentEnergy -= super.currentEnergy * 10 / 100 * super.activity;
    }
    /**
     * Спящий собакен.
     */
    public void sleep(){
        super.currentEnergy += super.maxEnergy * 5 / 100;
    }

    /**
     * Говорящий собакен.
     */
    public void talk(){
        System.out.println("Wow-wow-woooooooow...");
    }
}