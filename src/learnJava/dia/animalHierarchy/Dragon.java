
package learnJava.dia.animalHierarchy;

import learnJava.dia.animalHierarchy.Animal;

/**
 * Класс дракона, наследующий функциональность абстрактного вида животных Animal.
 *
 * @author D.I.A
 */

class Dragon extends Animal{

    public Dragon(String name, int maxEnergy, int minEnergy, int activity){
        super(name, maxEnergy, minEnergy, activity);
    }
    public Dragon(){
        super();
    }

    public String toString(){
        return super.toString();
    }

    /**
     * Дракон естъ.
     */
    public void eat(){
        super.currentEnergy += super.maxEnergy * 2 / 100;
    }
    /**
     * Дракон играетъ.
     */
    public void play(){
        super.currentEnergy -= super.currentEnergy / 100 * super.activity;
    }
    /**
     * Дракон спитъ.
     */
    public void sleep(){
        super.currentEnergy += super.maxEnergy / 100;
    }

    /**
     * Дракон говоритъ.
     */
    public void talk(){
        System.out.println("shhh-pff-GGGRRRRRGGHHAAAAAAAAAAAAFSHDRRR...");
    }
}