
package learnJava.dia.animalHierarchy;

import learnJava.dia.animalHierarchy.Cat;
import learnJava.dia.animalHierarchy.Dog;
import learnJava.dia.animalHierarchy.Dragon;


/**
 * Класс реализующий процесс существования животных.
 *
 * @author D.I.A
 */

public class AnimalDemo{
    public static void main(String[] args) {

        Animal animals[] = {
                new Cat("Murzillo", 100, 12, 10),
                new Dog("Sharik", 100, 32, 20),
                new Dragon("WorldDestroyer", 1000, 1, 80)
        };


        life(animals);

        printAnimalData(animals);

    }

    /**
     * Метод недолгого существования животных.
     *
     * @param animals - массив объектов подклассов суперкласса Animal
     */
    private static void life(Animal[] animals){
        int sleepAmount = 0;
        int playAmount = 0;

        for(int i = 0; i < animals.length; ++i){

            playAmount = 6 + (int)(Math.random() * 15);
            sleepAmount = 3 + (int)(Math.random() * 13);

            animals[i].talk();

            for(int p = 0; p < playAmount; ++p){
                animals[i].play();
            }
            for(int s = 0; s < sleepAmount; ++s){
                animals[i].sleep();
            }


            while (animals[i].isHungry()){
                animals[i].eat();
            }

        }

        Animal mostHungry = animals[0];
        for(int i = 1; i < animals.length; ++i){
            if(mostHungry.currentEnergy > animals[i].currentEnergy){
                mostHungry = animals[i];
            }
        }

        System.out.println("Самый голодный тут " + mostHungry.name);

    }

    /**
     * Выводит показатели животных.
     *
     * @param animals - массив объектов подклассов суперкласса Animal
     */
    private static void printAnimalData(Animal[] animals){
        for(Animal animal : animals){
            System.out.println(animal.toString());
        }
    }
}