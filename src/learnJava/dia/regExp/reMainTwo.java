
/**
 * reMainTwo.
 *
 * Класс выполняющий запись найденных идентификаторов в файле исходного кода
 * на языке Java. Добавлено удаление комментариев.
 *
 * @author D.I.A
 *
 */

package learnJava.dia.regExp;


import java.io.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;


public class reMainTwo{


    private static final String SRC_FILE_PATH = "C:\\Users\\DatLik\\IdeaProjects\\DIA_Code\\src\\learnJava\\dia\\regExp\\src.java";
    private static final String ID_FILE_PATH  = "C:\\Users\\DatLik\\IdeaProjects\\DIA_Code\\src\\learnJava\\dia\\regExp\\id.txt";
    private static final String REG_EXP = "\\b[a-zA-Z_]\\w*";


    public static void main(String[] args) {


        try{

            BufferedReader srcFile = new BufferedReader(new FileReader(SRC_FILE_PATH));
            BufferedWriter idFile  = new BufferedWriter(new FileWriter(ID_FILE_PATH));


            String lineBuf = null;
            String newLineBuf = null;

            Pattern pattern = Pattern.compile(REG_EXP);
            Matcher matcher = null;

            boolean isContinue = true;

            int openBracketIndex = -1;
            int closeBracketIndex = -1;

            while ((lineBuf = srcFile.readLine()) != null){

                if(isContinue){
                    openBracketIndex = lineBuf.indexOf("/*");
                }
                newLineBuf = lineBuf;

                if(openBracketIndex > -1){


                    newLineBuf = lineBuf.substring(0, openBracketIndex);


                    closeBracketIndex = lineBuf.lastIndexOf("*/");

                    if(closeBracketIndex > -1){

                        newLineBuf += lineBuf.substring(closeBracketIndex);
                        isContinue = true;

                    }else{

                        openBracketIndex = 0;
                        isContinue = false;

                    }

                }else{

                    newLineBuf = lineBuf.split("//")[0];

                }


                String[] ids = newLineBuf.split("\\W");


                for(int i = 0; i < ids.length; ++i){

                    matcher = pattern.matcher(ids[i]);

                    if(matcher.matches()){

                        System.out.println(ids[i]);

                        idFile.write(ids[i]);
                        idFile.newLine();

                    }

                }

            }

            srcFile.close();
            idFile.close();

        }catch(IOException ioError){

            ioError.printStackTrace();

        }

    }


}