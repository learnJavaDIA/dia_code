
package learnJava.dia.cats;

/**
 * Класс демонстрации работы класса...котиков...Cat.
 *
 * @author D.I.Aleksandrovich.
 */
public class CatDemo{

    public static void main(String[] args) {

        Cat cat = new Cat("Пушок", 'м', 1867);

        cat.mew();
        cat.eat();
        cat.walk();

    }
}