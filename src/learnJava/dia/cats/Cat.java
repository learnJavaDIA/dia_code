
package learnJava.dia.cats;

/**
 * Класс описывающий котиков...
 *
 * @author D.I.A
 */


class Cat{
    private String name;
    private char gender;
    private int birthYear;


    public Cat(String name, char gender, int birthYear){
        this.name = name;
        this.gender = gender;
        this.birthYear = birthYear;
    }
    public Cat(){
        this("Ghost", 'u', -1);
    }

    public String toString(){
        return "name " + this.name +
                "gender " + this.gender +
                "birthYear " + this.birthYear;
    }

    public String getName() {
        return name;
    }

    public char getGender() {
        return gender;
    }

    public int getBirthYear() {
        return birthYear;
    }


    /**
     * Метод дающий возможность поговорить с котом.
     */
    public void mew(){
        System.out.println("Ммммяяяяяуууу...");
    }

    /**
     * Метод поглощения пищи котярой.
     */
    public void eat(){
        System.out.println("Грх-нргхр-хрр-гррр...");
    }

    /**
     * Метод риказывает котяре валить гулять.
     */
    public void walk(){
        System.out.println("Уахаха, пришло время драться...");
    }
}