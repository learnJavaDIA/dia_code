
/**
 * Интерфейс содержащий абстрактные методы действий робота.
 *
 * @author D.I.A
 */


package learnJava.dia.testingInterfaces.interfaces;

public interface Action{
    void toTake();
    void toThrow();
}