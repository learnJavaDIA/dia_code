
/**
 * Интерфейс содержащий абстрактные методы передвижения робота.
 *
 * @author D.I.A
 */


package learnJava.dia.testingInterfaces.interfaces;

public interface Move{
    void toForward();
    void toRotate();
}