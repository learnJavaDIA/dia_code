
/**
 * Интерфейс содержащий абстрактные методы работы датчиков робота.
 *
 * @author D.I.A
 */


package learnJava.dia.testingInterfaces.interfaces;

public interface Detect{
    boolean isDetectObstacle();
    boolean isDetectItem();
}