
/**
 * Класс робота, реализующий интерфейсы.
 *
 * @author D.I.A
 */

package learnJava.dia.testingInterfaces;

import learnJava.dia.testingInterfaces.interfaces.Move;
import learnJava.dia.testingInterfaces.interfaces.Detect;
import learnJava.dia.testingInterfaces.interfaces.Action;

import learnJava.dia.Point2D;
import learnJava.dia.Vector2D;


class Robot implements Move, Detect, Action{
    private Point2D position;
    private Vector2D direction;
    private int inventoryCount;

    public Robot(Point2D position, Vector2D direction){
        this.position = position;
        this.direction = direction;
    }
    public Robot(){
        this(new Point2D(0, 0), new Vector2D(1, 0));
    }

    @Override
    public String toString(){
        return String.format("Position %s\nDirection %s\nInventory Count %d", position.toString(), direction.toString(), inventoryCount);
    }


    /**
     * Метод выполняет движение на 1 шаг в направлении direction.
     */
    @Override
    public void toForward(){
        if(!isDetectObstacle()){
            position.x += direction.x;
            position.y += direction.y;
        }else {
            toRotate();
        }

        if(isDetectItem()){
            toTake();
        }
    }

    /**
     * Метод выполняет поворот на 90 градусов по часовой стрелке.
     */
    @Override
    public void toRotate(){
        int x = direction.x;
        int y = direction.y;
        direction.x = y;
        direction.y = -x;
    }

    /**
     * Метод "взятия предмета".
     */
    @Override
    public void toTake(){
        ++inventoryCount;
    }

    /**
     * Метод "удаления предмета".
     */
    @Override
    public void toThrow(){
        --inventoryCount;
    }

    /**
     * Метод определения препятствий перед роботом.
     *
     * @return true если перед роботом препятствие, иначе false.
     */
    @Override
    public boolean isDetectObstacle(){
        return ((int)(Math.random() * 2) > 0);
    }

    /**
     * Метод определения предметов перед роботом.
     *
     * @return true если перед роботом находится предмет, иначе false.
     */
    @Override
    public boolean isDetectItem(){
        return ((int)(Math.random() * 2) > 0);
    }
}