/**
 * Класс демонстрации работы класса Robot
 *
 * @author D.I.A
 */

package learnJava.dia.testingInterfaces;

import learnJava.dia.testingInterfaces.Robot;


public class RobotDemo{
    public static void main(String[] args) {
        Robot robot = new Robot();

        for (int i = 0; i != 24; ++i){
            robot.toForward();
        }

        System.out.println(robot.toString());
    }
}