

package learnJava.dia.matrix;


public class Matrix{

    public static void main(String[] args) {

        int size = 4 + (int)(Math.random() * 6);
        int quadSize = size / 2;
        int matrix[][] = new int[size][size];

        matrixRandowInit(matrix, -10, 10);
        printMatrix(matrix);

        int sum = matrixSum(matrix, 0, quadSize, quadSize - 1, size - 1) + matrixSum(matrix, quadSize, 0, size - 1, quadSize - 1);
        System.out.printf("Сумма выбранных элементов матрицы = %d", sum);

    }


    private static void printMatrix(int matrix[][]){

        for(int r = 0, c = 0; r < matrix.length; ++r){

            while (c <  matrix[r].length){

                System.out.printf("[%d]", matrix[r][c]);
                ++c;

            }

            c = 0;

            System.out.println();

        }


    }

    private static void matrixRandowInit(int matrix[][], int min, int max){

        max = (min < 0 ? (max + (-min)) : (max - min));

        for(int r = 0, c = 0; r < matrix.length; ++r){

            while (c <  matrix[r].length){

                matrix[r][c] = min + (int)(Math.random() * max);
                ++c;

            }

            c = 0;

        }

    }

    /**
     * Метод находит сумму элементов матрицы в "выделенной области",
     * от начального (левого, верхнего) элемента до конечного (правого, нижнего) элемента.
     *
     * @param matrix - матрица сумма элементов которой должна быть вычислена.
     * @param rowBegin - строка начального элемента.
     * @param colBegin - столбец начального элемента.
     * @param rowEnd - строка конечного элемента.
     * @param colEnd - столбец конечного элемента.
     * @return возвращает сумму элементов матрицы в выделенной области.
     */
    private static int matrixSum(int matrix[][], int rowBegin, int colBegin, int rowEnd, int colEnd){

        int sum = 0;

        for(int r = rowBegin, c = colBegin; r <= rowEnd; ++r){

            while (c <= colEnd){

                sum += matrix[r][c];
                ++c;

            }

            c = colBegin;

        }

        return sum;

    }


}