package dia.multiplyNotOperator;

public class Main {

    public static void main(String[] args){

        if(UnitTest(1000)){

            System.out.println("done!");

        }else {

            System.out.println("error!");

        }

    }

    private static boolean UnitTest(int count){

        int a = 0;
        int b = 0;

        for(int i = 0; i < count; ++i){

            a = -100 + (int)(Math.random() * 200);
            b = -100 + (int)(Math.random() * 200);

            if(a * b != multi(a, b)){

                return  false;

            }

        }

        return true;

    }

    private static int multi(int a, int b){

        if(a == 0 || b == 0){

            return 0;

        }

        int result   = 0;
        int incValue = 0;
        int count    = 0;
        int sign     = 1;

        if(a < 0){

            a = -a;
            sign = -sign;

        }
        if(b < 0){

            b = -b;
            sign = -sign;

        }


        if(a < b){

            incValue = b;
            count    = a;

        }else{

            incValue = a;
            count    = b;

        }

        for(int i = 0; i < count; ++i){

            result += incValue;

        }

        return (sign < 0 ? -result : result);

    }

}
