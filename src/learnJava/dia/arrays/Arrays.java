

package learnJava.dia.arrays;


public class Arrays{


    private static final String FORMAT_ARRAY_NUMBER = "Массив чисел кратных числу %d\n";


    public static void main(String[] args) {

        int srcArray[] = new int[32];
        arrayRandomInit(srcArray, 1, 100);

        int three[] = new int[srcArray.length];
        int five[] = new int[srcArray.length];
        int threeAndFive[] = new int[srcArray.length * 2];


        for(int i = 0, t = 0, f = 0, tf = 0; i < srcArray.length; ++i){

            if(srcArray[i] % 3 == 0){

                three[t] = srcArray[i];
                ++t;

                if(srcArray[i] % 5 == 0){

                    threeAndFive[tf] = srcArray[i];
                    ++tf;

                }

            }else if(srcArray[i] % 5 == 0){

                five[f] = srcArray[i];
                ++f;

                if(srcArray[i] % 3 == 0){

                    threeAndFive[tf] = srcArray[i];
                    ++tf;

                }

            }

        }


        System.out.println("Исходный массив");
        printArray(srcArray);

        System.out.printf(FORMAT_ARRAY_NUMBER, 3);
        printArray(three);

        System.out.printf(FORMAT_ARRAY_NUMBER, 5);
        printArray(five);

        System.out.println("Массив чисел кратных числу 3 и 5");
        printArray(threeAndFive);


    }


    private static void printArray(int array[]){

        if(array[0] == 0){

            System.out.println("Array is empty!");

        }else {

            for(int i = 0; i < array.length && array[i] != 0; ++i){

                System.out.printf("[%d]", array[i]);

            }

            System.out.println();

        }

    }

    private static void arrayRandomInit(int array[], int min, int max){

        max = min < 0 ? max + (-min) : max - min;

        for(int i = 0; i < array.length; ++i){

            array[i] = min + (int)(Math.random() * max);

        }

    }

}