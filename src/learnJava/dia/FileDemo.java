
package learnJava.dia;

import java.io.FileInputStream;
import java.io.IOException;


public class FileDemo{

    public static void main(String[] args) {
        try {

            byte[] bytes = readBytesFromFile("C:\\Users\\DatLik\\IdeaProjects\\DIA_Code\\src\\learnJava\\dia\\testFile.txt");

        }catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    private static byte[] readBytesFromFile(String file) throws IOException{
        FileInputStream fis = new FileInputStream(file);

        for(int i = -1; (i = fis.read()) != -1; ){
            System.out.println(i);
        }

        byte[] bytes = new byte[fis.available()];
        fis.read(bytes);
        fis.close();
        return bytes;
    }

}